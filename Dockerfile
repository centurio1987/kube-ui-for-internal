FROM node
COPY . /keyonbit-ui
WORKDIR /keyonbit-ui
ENTRYPOINT npm run start