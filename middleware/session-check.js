export default async (context) => {
  try {
    const res = await context.$axios.get('/api/auth/session/check')
    if(context.store.getters['user/isAuthenticated']){
      context.store.commit('user/authenticated')
    }
    else {
      context.store.commit('user/unauthenticated')
      context.redirect('/login')
    }
  } catch (err) {
    console.log(err)
  }
}