const express = require('express')
const router = express.Router()
const queryDb =  require('./db/query_db')
const queryAuth = require('./auth/auth')
const test = require('./test')
const sessionCheck = require('./session_check')

router.use('/data', queryDb)
router.use('/auth', queryAuth)
router.use('/test', test)
router.use('/session/check', sessionCheck)
module.exports = router