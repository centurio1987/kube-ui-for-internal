const express = require('express')
const router = express.Router()
const passport = require('passport')
const local = require('./local')
const { Pool } = require('pg')
const connectionString = "postgresql://keyonbit:key0nb1t@stolon.internal.keyonbit.com/keyonbit_ui"
const pool = new Pool({
  connectionString: connectionString
})

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser(async (id, done) => {
  try {
    const {rows} = await pool.query("select id from users where id=$1", [id])
    if(rows[0])
      done(null, rows[0])
    else
      done(err)
  } catch(err) {
    done(err)
  }
})

router.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true
  })
)

router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

router.get('/session/check', (req, res) => {
  res.send(req.user)
})

router.post('/signup', async (req, res) => {
  const { username, password, salt } = req.body
  try{
    const { rows } = await pool.query("select username from users where username = $1", [username])
    if (!rows[0]){
      await pool.query('insert into users(id, username, password, salt) values(default, $1, $2, $3)', [username, password, salt])
      res.send("Sign Up Success!!")
    } else{
      res.send("User Name is already exist!!")
    }
    await pool.end()
  } catch(err){
    console.log(err)
  }
})

module.exports = router