const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
const CryptoJS = require('crypto-js')
const { Pool } = require('pg')
const connectionString = "potgresql://keyonbit:key0nb1t@stolon.internal.keyonbit.com:5432/keyonbit_ui"
const pool = new Pool({
  connectionString: connectionString
})

passport.use(new LocalStrategy(
  function(username, password, done){
    pool.query("select * from users where username=$1", [username]).then(res => {
      if(!res.rows[0]){
        return done(null, false, {message: "Incorrect Username"})
      }
      const salt = CryptoJS.enc.Base64.parse(res.rows[0].salt)
      const encryptedPassword = CryptoJS.PBKDF2(password, salt, {
        keySize: 512 / 32,
        iterations: 1000
      })
      const base64EncPassword = CryptoJS.enc.Base64.stringify(encryptedPassword)
      if(!(res.rows[0].password === base64EncPassword )){
        return done(null, false, {message: "Incorrect Password"})
      }
      return done(null, res.rows[0])
    })
    .catch(err =>{
      return done(err)
    })
  }
))