const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()
const session = require('express-session')
const redis = require('redis')
const RedisStore = require('connect-redis')(session)
const passport = require('passport')
const flash = require('connect-flash')

// session obj setting
let redisClient = redis.createClient({
  host: "redis.internal.keyonbit.com",
  port: 6380,
  password: "key0nb1t"
})

let sess = {
  secret: "bit by bit",
  rolling: true,
  resave: false,
  saveUninitialized: false,
  store: new RedisStore({ client: redisClient }),
  cookie: {
    maxAge: 1800000
  }
}

if(app.get("env") === 'production'){
  sess.cookie.secure = true;
}

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(session(sess))
  app.use(express.json()) // for parsing application/json
  app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
  app.use(passport.initialize())
  app.use(passport.session())
  app.use(flash())
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
