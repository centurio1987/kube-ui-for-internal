const express = require('express')
const router = express.Router()
const { Pool } = require('pg')
const connectionString = 'postgresql://keyonbit:key0nb1t@stolon.internal.keyonbit.com:5432/keyonbit_ui'
const pool = new Pool({
    connectionString: connectionString
})

router.get('/:name/', async (req, res) => {
    const { name } = req.params
    try {
        const { rows } = await pool.query('select url from registered_services where name=$1', [name])
        res.send(rows[0])
    } catch (err) {
        console.log(err)
    }
});

module.exports = router;