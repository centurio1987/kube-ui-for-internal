
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#0000FF' },
  /*
  ** Global CSS
  */
  css: [{ src: '~assets/scss/main.scss', lang: 'scss' },
    {src: "node_modules/@fortawesome/fontawesome-free/scss/fontawesome.scss", lang: "scss"},
    {src: "node_modules/@fortawesome/fontawesome-free/scss/solid.scss", lang: "scss"},
    {src: "node_modules/@fortawesome/fontawesome-free/scss/regular.scss", lang:"scss"},
    {src: "node_modules/@fortawesome/fontawesome-free/scss/brands.scss", lang:"scss"},
    {src: "~assets/scss/kubernetes-icon.scss", lang:"scss"}
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  /*
  ** Build configuration
  */
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    },
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  serverMiddleware: [
    {path: '/api', handler:'~/server/query.js'}
  ],
  router: {
    middleware: ['session-check']
  },
  // auth: {
  //   strategies: {
  //     local: {
  //       endpoints: {
  //         login: { url: '/v1/auth/login', method: 'post', propertyName: 'data.token' }
  //       }
  //     }
  //   }
  // }
}
