# kube-ui-for-internal

> Control Center for Keyonbit Private Kubernetes Cluster

## Build Setup
https://drone.internal.keyonbit.com/api/badges/kimyoondeok/keyonbit-ui/status.svg

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

# happy vueing!!
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
