export default {
  add: (state, user) => {
    state.user.username = user.username
  },
  authenticated: (state) => {
    state.isAuthenticated = true
  },
  unauthenticated: (state) => {
    state.isAuthenticated = false
  }
}